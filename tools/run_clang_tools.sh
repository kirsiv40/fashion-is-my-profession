#!/bin/bash
echo "Running clang-format on all cpp and h files by executing: find cpp/ -name '*.cpp' -or -name '*.h' | xargs clang-format -i"
find cpp/ -name '*.cpp' -or -name '*.h' | xargs clang-format -i