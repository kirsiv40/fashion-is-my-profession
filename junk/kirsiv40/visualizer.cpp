#include "visualizer.h"

#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <string>
#include <iostream>
#include <fstream>
#include <chrono>

#include <QThread>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QLabel>
#include <QtWidgets/QStylePainter>
#include <QTextEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QCheckBox>
#include <QRadioButton>

constexpr int w = 369;
constexpr int h = 800;


void WorkerThread::SetUp(std::string p1, std::string p2, std::string clpos) {
    path1 = p1;
    path2 = p2;
    cpos = clpos;
}

void WorkerThread::run() {
    int aaaa;
    int x = fork();
    if (x == 0) {
        execl("junk/kirsiv40/script", " ", path1.c_str(), path2.c_str(), "resultpath", cpos.c_str());
    }
    ::wait(&aaaa);
    std::cout << "neuro ended" << std::endl;
}


PositionChooseButtons::PositionChooseButtons(QWidget* parent) : QWidget(parent) {
}

void PositionChooseButtons::SetUp() {
    top = new QRadioButton(this);
    bot = new QRadioButton(this);
    dress = new QRadioButton(this);

    top->setGeometry(0, 0, 100, 20);
    bot->setGeometry(125, 0, 100, 20);
    dress->setGeometry(250, 0, 100, 20);

    top->setText("Upper-body");
    bot->setText("Lower-body");
    dress->setText("Dress");

    top->setChecked(true);
}

CleverLabel::CleverLabel(QWidget* parent, QPushButton* other, PositionChooseButtons* chk) : 
            label(new ChooseLabel(parent)), label2(new ChooseLabel(parent)), thr(new WorkerThread()),
            but(other), neuroresultimage(new NeuroResult(this, parent)), cb(chk) {
}

void CleverLabel::SetUpConnections() {
    QObject::connect(thr, &WorkerThread::finished, this, &CleverLabel::NeuroHandler);
}

void CleverLabel::ChangeNeuroValues(int st, int gSc, int sd) {
    gScale = gSc;
    steps = st;
    seed = sd;
}

void CleverLabel::SetUp() {
    label->SetUp(label, &fileName);
    label2->SetUp(label2, &fileName2);

    label->setText(QT_VERSION_STR);
    label->setScaledContents(true);
    label->resize(w / 2, w / 2);
    label->setPixmap(QPixmap("junk/kirsiv40/icons/man-icon.png"));

    label2->setScaledContents(true);
    label2->resize(w / 2, w / 2);
    label2->move(w / 2, 0);
    label2->setPixmap(QPixmap("junk/kirsiv40/icons/cloth-icon.png"));

    neuroresultimage->setGeometry(14, 189, 345, 460);
    neuroresultimage->setScaledContents(true);
    neuroresultimage->setText(QT_VERSION_STR);
    neuroresultimage->setPixmap(QPixmap("junk/kirsiv40/icons/background.jpg"));

    neuroresultimage->setAlignment(Qt::AlignCenter);
    neuroresultimage->setFont(QFont("Sans Serif", 20));

    gScale = 300;
    steps = 30;
    seed = -1;

    SetUpConnections();
}

void CleverLabel::ShowImage() {
    if (!fileName.isEmpty() && !fileName2.isEmpty()) {
        but->setEnabled(false);
        if (cb->top->isChecked()) {
            thr->SetUp(fileName.toStdString().c_str(), fileName2.toStdString().c_str(), std::string("Upper-body") + " " + std::to_string(steps) + " " + std::to_string(gScale) + " " + std::to_string(seed));
        } else if (cb->bot->isChecked()) {
            thr->SetUp(fileName.toStdString().c_str(), fileName2.toStdString().c_str(), std::string("Lower-body") + " " + std::to_string(steps) + " " + std::to_string(gScale) + " " + std::to_string(seed));
        } else {
            thr->SetUp(fileName.toStdString().c_str(), fileName2.toStdString().c_str(), std::string("Dress") + " " + std::to_string(steps) + " " + std::to_string(gScale) + " " + std::to_string(seed));
        }
        thr->start();
    } else {
        //================================================================================================
    }
}

void CleverLabel::NeuroHandler() {
    std::string path;
    std::ifstream fin("resultpath");
    fin >> path;
    fin.close();
    if (path == "BADFILES") {
        neuroresultimage->setText("Bad Input Images");
    } else {
        resultpath = path;
        neuroresultimage->setPixmap(QString(path.data()));
    }
    but->setEnabled(true);
}

void NeuroResult::mouseDoubleClickEvent(QMouseEvent* e) {
    if (!_parent->resultpath.empty()) {
        _parent->fileName = QString(_parent->resultpath.c_str());
        _parent->label->setPixmap(QPixmap(_parent->fileName));
    }
}

NeuroResult::NeuroResult(CleverLabel* parentCl, QWidget* parent) : QLabel(parent) {
    _parent = parentCl;
}

ImageOpenerButton::ImageOpenerButton(QWidget* parent) : QPushButton(parent) {
}

void ImageOpenerButton::SetUpConnections() {
    QObject::connect(this, &QPushButton::clicked, this, &ImageOpenerButton::HandlePress);
}

void ImageOpenerButton::SetUp(QLabel* target, QString* target2) {
    _targetimage = target;
    _targetpath = target2;

    SetUpConnections();
}

void ImageOpenerButton::HandlePress() {
    QString fileName = QFileDialog::getOpenFileName(this->parentWidget(), tr("Open Image"), "/home", tr("Image Files (*.png *.jpg *.bmp *.jpeg)"));
    if (!fileName.isEmpty()) {
        _targetimage->setPixmap(fileName);
        *_targetpath = fileName;
    }
}

ChooseLabel::ChooseLabel(QWidget* parent) : QLabel(parent) {
}

void ChooseLabel::SetUpConnections() {
}

void ChooseLabel::SetUp(QLabel* target, QString* target2) {
    _targetimage = target;
    _targetpath = target2;

    SetUpConnections();
}

void ChooseLabel::mousePressEvent(QMouseEvent* /*unused*/) {
    QString fileName = QFileDialog::getOpenFileName(this->parentWidget(), tr("Open Image"), "/home", tr("Image Files (*.png *.jpg *.bmp *.jpeg)"));
    if (!fileName.isEmpty()) {
        _targetimage->setPixmap(fileName);
        *_targetpath = fileName;
    }
}


ValLabel::ValLabel(QWidget* parent) : QLabel(parent) {
}

void ValLabel::SetValue(int val) {
    if (val == -1) {
        setText("Random");
    } else {
        setNum(val);
    }
}

SettingsScreen::SettingsScreen(QWidget* parent) : QWidget(parent) {
}

void SettingsScreen::SetUpConnections() {
    QObject::connect(par1, &QSlider::valueChanged, vallab1, &ValLabel::SetValue);
    QObject::connect(par2, &QSlider::valueChanged, vallab2, &ValLabel::SetValue);
    QObject::connect(par3, &QSlider::valueChanged, vallab3, &ValLabel::SetValue);
    QObject::connect(submit, &QPushButton::clicked, this, &SettingsScreen::ChangeValues);
}

void SettingsScreen::SetUp(CleverLabel* cl) {
    _cl = cl;
    
    par1 = new QSlider(this);
    par2 = new QSlider(this);
    par3 = new QSlider(this);

    lab1 = new QLabel(this);
    lab2 = new QLabel(this);
    lab3 = new QLabel(this);

    vallab1 = new ValLabel(this);
    vallab2 = new ValLabel(this);
    vallab3 = new ValLabel(this);

    submit = new QPushButton(this);

    par1->setOrientation(Qt::Horizontal);
    par1->setGeometry(20, 30, w - 60, 20);
    par1->setMinimum(20);
    par1->setMaximum(40);
    par1->setValue(30);

    par2->setOrientation(Qt::Horizontal);
    par2->setGeometry(20, 100, w - 60, 20);
    par2->setMinimum(100);
    par2->setMaximum(500);
    par2->setValue(300);

    par3->setOrientation(Qt::Horizontal);
    par3->setGeometry(20, 170, w - 60, 20);
    par3->setMinimum(-1);
    par3->setMaximum(214748);
    par3->setValue(-1);

    lab1->setGeometry(20, 10, w - 120, 20);
    lab1->setText("Steps");

    lab2->setGeometry(20, 80, w - 120, 20);
    lab2->setText("Guidance scale");

    lab3->setGeometry(20, 150, w - 120, 20);
    lab3->setText("Seed");

    vallab1->setGeometry(w - 100, 10, 60, 20);
    vallab1->setNum(par1->value());
    vallab1->setAlignment(Qt::AlignRight);

    vallab2->setGeometry(w - 100, 80, 60, 20);
    vallab2->setNum(par2->value());
    vallab2->setAlignment(Qt::AlignRight);

    vallab3->setGeometry(w - 100, 150, 60, 20);
    vallab3->setText("Random");
    vallab3->setAlignment(Qt::AlignRight);

    submit->setGeometry(20, 210, w - 60, 40);
    submit->setText("Submit");

    SetUpConnections();
}

void SettingsScreen::ChangeValues() {
    _cl->ChangeNeuroValues(par1->value(), par2->value(), par3->value());
    hide();
}


void Screen::SetUpConnections() {
    QObject::connect(but, &QPushButton::clicked, cl, &CleverLabel::ShowImage);
    QObject::connect(settings, &QPushButton::clicked, settingsScreen, &SettingsScreen::Open);
}

void Screen::SetUp() {
    setWindowTitle("Outfit Manager");

    but = new QPushButton(this);
    settings = new QPushButton(this);
    ps = new PositionChooseButtons(this);

    cl = new CleverLabel(this, but, ps);

    firstimagebut = new ImageOpenerButton(this);
    secondimagebut = new ImageOpenerButton(this);

    cl->SetUp();

    but->move(10, 655);
    but->resize(305, 40);
    but->setEnabled(true);
    but->setText("Generate");

    settings->setGeometry(318, 655, 40, 40);
    settings->setIcon(QIcon(QPixmap("junk/kirsiv40/icons/set-icn.png")));
    settings->setIconSize(QSize(30, 30));

    firstimagebut->SetUp(cl->label, &cl->fileName);
    secondimagebut->SetUp(cl->label2, &cl->fileName2);

    firstimagebut->setGeometry(10, w / 2 + 10, w / 2 - 30, h / 40);
    secondimagebut->setGeometry(w / 2 + 10, w / 2 + 10, w / 2 - 20, h / 40);

    firstimagebut->setText("Person");
    secondimagebut->setText("Cloth");

    firstimagebut->hide();
    secondimagebut->hide();

    ps->setGeometry(10, 700, 349, 20);
    ps->SetUp();

    settingsScreen = new SettingsScreen(this);
    settingsScreen->setGeometry(10, 500, w - 20, h - 100);
    settingsScreen->setAutoFillBackground(true);
    settingsScreen->SetUp(cl);

    settingsScreen->hide();

    SetUpConnections();
}

